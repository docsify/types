### TypeScript definitions for undocumented modules.

Dependencies:
  - @types/amqplib
  - @types/bunyan
  - @types/http-errors
  - @types/ioredis
  - @types/joi
  - @types/koa
  - @types/mongoose
  - @types/node

Install dependencies:
```bash
$ yarn add -D @types/amqplib @types/bunyan @types/http-errors @types/ioredis @types/joi @types/koa @types/mongoose @types/node
```