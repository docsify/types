/// <reference types="koa" />
/// <reference types="http-errors" />

declare module 'koa-2-error-handler' {
  import { Context } from 'koa';
  import { HttpError } from 'http-errors';
  interface IErrorResponse extends HttpError {
    error: any;
    stack?: any;
  }
  export function error(f: (err: IErrorResponse, ctx: Context) => any): any;
}
