/// <reference types="koa" />

declare module 'koa-ctx-paginate' {
  import { Middleware } from 'koa';
  export function middleware(limit: number, maxLimit: number): Middleware;
}
