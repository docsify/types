declare module 'config' {
  interface IConfig {
    amqp:{
      exchange: string;
      queue: string;
      defferedQueue: string;
      url: string;
      options: {
        delay: number;
      },
    },
    cookeis: {
      name: string;
    },
    debugKey: string;
    google: {
      clientId: string,
      clientSecret: string,
      redirectUrl: string
    },
    jwt: {
      algorithm: string,
      expiresIn: number,
      secret: string
    },
    logger: {
      count: number;
      period: string;
      src: boolean;
      level: number;
      debug: boolean;
    },
    logsPath: string,
    mongo: {
      host: string
    },
    redis: {
      url: string
    },
    redisAuthKey: string,
    server: {
      host: string,
      port: number
    }
  }
}
