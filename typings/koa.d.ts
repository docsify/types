/// <reference types="koa" />
/// <reference types="bunyan" />
/// <reference types="mongoose" />
/// <reference types="ioredis" />
/// <reference types="ioredis" />
/// <reference path="./amqp-rpc.d.ts" />

// import { IRPCClient } from '@elastic.io/amqp-rpc';
// import { ReadStream } from 'fs';
// import * as Koa from 'koa';
// import * as bunyan from 'bunyan';
// import { Mongoose } from 'mongoose';
// import { Redis } from 'ioredis';
import { StackFrame } from 'error-stack-parser';

// export interface MyContext {
//   authUser?: any;
//   log: bunyan;
//   paginate: {
//     skip: number;
//     offset: number;
//   },
//   state: {
//     paginate: {
//       page: number;
//       limit: number;
//       href: string;
//       hasPreviousPages: boolean;
//       hasNextPages: boolean;
//       getArrayPages(): number[];
//     };
//   };
//   data: any,
//   redis: Redis,
//   mongoose: Mongoose,
//   rpcClient: IRPCClient
// }

declare module 'koa' {
  export interface IPaginate {
    page: number;
    limit: number;
    href: string;
    hasPreviousPages: boolean;
    hasNextPages: boolean;
  }

  export interface IResponseBody {
    statusCode: number;
    message: string;
    data?: any;
    error?: any;
    stack?: any;
  }

  export interface IErrorResponse {
    error: string;
    message: string;
    statusCode: number;
    stack?: StackFrame[];
  }

  // export interface Context {
  //   body: | IResponseBody | ReadStream | string;
  // }
  // export interface Context extends MyContext {
  //   body: | IResponseBody | ReadStream | string;
  // }
}
