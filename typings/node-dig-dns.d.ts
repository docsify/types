declare module 'node-dig-dns' {
  interface IDig {
		<TResult>(
      args: string[],
      options?: {
        raw?: string;
        dig?: string;
      }
		): any;
	}

  var dig: IDig;

  export = dig;
}
