/// <reference types="koa" />
/// <reference types="joi" />

declare module 'koa-context-validator' {
  import { Middleware } from 'koa';
  import { ValidationOptions } from 'joi';
  export * from 'joi';
  export default function validator(rules: { body?: object, query?: object, headers?: object}, options: ValidationOptions): Middleware;
}
