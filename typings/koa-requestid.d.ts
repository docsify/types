/// <reference types="koa" />

declare module 'koa-requestid' {
  import { Middleware } from 'koa';

  interface IKoaRequestId {
    (options?: { expose?: string, header?: string, query?: string }): Middleware;
  }

  const KoaRequestId: IKoaRequestId;

  export = KoaRequestId;
}
