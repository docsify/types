/// <reference types="amqplib" />

declare module '@elastic.io/amqp-rpc' {
  import { Connection } from 'amqplib';

  interface IRPCParams {
    token?: string;
    apikey?: string;
    roles?: string[];
    data?: any;
  }

  interface IRPCResponse {
    result?: any;
    message?: any;
    code?: any;
    columnNumber?: any;
    error?: {
      code: number;
      message: string;
    };
  }

  interface IRPCServer {
    start(): Promise<void>;
    addCommand(name: string, handler: (params: IRPCParams) => Promise<IRPCResponse>): void;
  }

  interface IRPCClient {
    start(): Promise<void>;
    sendCommand(name: string, params: IRPCParams[]): Promise<IRPCResponse>;
  }

  interface IParams {
    requestsQueue: string;
  }

  export const AMQPRPCServer: {
    new (connection: Connection, params: IParams): IRPCServer
  }

  export const AMQPRPCClient: {
    new (connection: Connection, params: IParams): IRPCClient
  }
}

