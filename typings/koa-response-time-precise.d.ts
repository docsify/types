/// <reference types="koa" />

declare module 'koa-response-time-precise' {
  import { Middleware } from 'koa';

  interface IKoaResponseTimePrecise {
    (): Middleware;
  }

  const KoaResponseTimePrecise: IKoaResponseTimePrecise;

  export = KoaResponseTimePrecise;
}
