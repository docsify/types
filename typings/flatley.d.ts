declare module 'flatley' {
	interface FlattenOptions {
		delimiter?: string;
		safe?: boolean;
		maxDepth?: number;
		coercion?: any;
	}

	interface Flatten {
		<TTarget, TResult>(
			target: TTarget,
			options?: FlattenOptions
		): TResult;

		flatten: Flatten;
		unflatten: Unflatten;
	}

	interface UnflattenOptions {
		delimiter?: string;
		object?: boolean;
		overwrite?: boolean;
	}

	interface Unflatten {
		<TTarget, TResult>(
			target: TTarget,
			options?: UnflattenOptions
		): TResult;
	}

  var flatten: Flatten;

  export = flatten;
  // export default Flatten;
}
