declare module 'gelf-pro' {
  interface IGelfProConfig {
    fields?: object; // optional; default fields for all messages
    filter?: ((msg: any) => boolean)[]; // optional; filters to discard a message
    transform?: ((msg: any) => any)[]; // optional; transformers for a message
    broadcast?: ((msg: any) => void)[]; // optional; listeners of a message
    levels?: {
      emergency?: number;
      alert?: number;
      critical?: number;
      error?: number;
      warning?: number;
      notice?: number;
      info?: number;
      debug?: number;
    }; // optional; default: see the levels section below
    aliases?: object; // optional; default: see the aliases section below
    adapterName?: 'udp' | 'tcp' | 'tcp-tlc', // optional; currently supported "udp", "tcp" and "tcp-tls"; default: udp
    adapterOptions?: { // this object is passed to the adapter.connect() method
      host?: string; // optional; default: 127.0.0.1
      port?: number; // optional; default: 12201
      // tcp adapter example
      family?: number; // tcp only; optional; version of IP stack; default: 4
      timeout?: number; // tcp only; optional; default: 10000 (10 sec)
      // udp adapter example
      protocol?: 'udp4' | 'udp6' | 'udp4';
      // tcp-tls adapter example
      key?: Buffer; // tcp-tls only; optional; only if using the client certificate authentication
      cert?: Buffer; // tcp-tls only; optional; only if using the client certificate authentication
      ca?: Buffer; // tcp-tls only; optional; only for the self-signed certificate
    }
  }

  interface IGelfPro {
		setConfig: (config: IGelfProConfig) => void;
		emergency: (...params: any) => void;
		alert: (...params: any) => void;
		critical: (...params: any) => void;
		error: (...params: any) => void;
		warning: (...params: any) => void;
		notice: (...params: any) => void;
		info: (...params: any) => void;
		debug: (...params: any) => void;
	}

  var gelf: IGelfPro;

  export = gelf;
}
